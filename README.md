# OS161
### About
[OS161](http://os161.eecs.harvard.edu/) is an educational operating system developed by the Systems Research Group at [Harvard University](http://www.harvard.edu/). It aims to strike a balance between giving students experience working on a real operating system, and potentially overwhelming students with the complexity that exists in a fully fledged operating system, such as Linux. Compared to most deployed operating systems, OS161 is quite small (approximately 20,000 lines of code), and therefore it is much easier to develop an understanding of the entire code base.
The complete OS161 environment consists of three major parts:
- OS161 itself;
- System161, the simulated machine that OS/161 runs on;
- development tools required to compile and debug code for the simulated machine.

### Implemented Features
* Synchronization Primitives
  * Locks
  * Condition Variables
* Traffic Intersection Problem
* System Calls
  * fork
  * getpid
  * waitpid
  * _exit
  * execv with argument passing
* runprogram with argument passing
* Physical Memory Management

### License
* OS161 is licensed under this [license](https://www.github.com/elailai94/OS161/blob/master/LICENSE.md).
