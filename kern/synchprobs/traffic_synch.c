#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution. Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables. You are also free to 
 * declare other global variables if your solution requires them.
 */


/*
 * Synchronization and other variables.
 */
/* Vehicles in the intersection */
/* 
 * Note: this is an array of unsigned integers with size 12.
 * Each element in the array stores the number of cars heading in
 * a particular direction.
 */
const unsigned int num_directions = 12;
Direction directions[12][2] =
{
  { north, south },
  { north, west  },
  { north, east  }, 
  { east , west  },
  { east , north },
  { east , south },
  { south, north },
  { south, west  },
  { south, east  },
  { west , east  },
  { west , north },
  { west , south }
};
unsigned int vehicles_in_intersection[12] = {0};
/* Lock to provide mutual exclusion */
struct lock *mutex = NULL;
/* Condition variables */
struct cv *synch_conditions_met = NULL;
struct cv *north_south_can_enter = NULL;
struct cv *north_west_can_enter = NULL;
struct cv *north_east_can_enter = NULL;
struct cv *east_west_can_enter = NULL;
struct cv *east_north_can_enter = NULL;
struct cv *east_south_can_enter = NULL;
struct cv *south_north_can_enter = NULL;
struct cv *south_west_can_enter = NULL;
struct cv *south_east_can_enter = NULL;
struct cv *west_east_can_enter = NULL;
struct cv *west_north_can_enter = NULL;
struct cv *west_south_can_enter = NULL;


/*
 * This function increments the element in the array that stores
 * the number of cars heading in a particular direction.
 */

static void
enter_intersection(Direction origin, Direction destination)
{
  switch (origin) {
    
    case north:
      switch (destination) {
        case south:
          ++(vehicles_in_intersection[0]);
          break;
        case west:
          ++(vehicles_in_intersection[1]);
          break;
        case east:
          ++(vehicles_in_intersection[2]);
          break;
        case north:
          break;
      }
      break;
    
    case east:
      switch (destination) {
        case west:
          ++(vehicles_in_intersection[3]);
          break;
        case north:
          ++(vehicles_in_intersection[4]);
          break;
        case south:
          ++(vehicles_in_intersection[5]);
          break;
        case east:
          break;
      }
      break;

    case south:
      switch (destination) {
        case north:
          ++(vehicles_in_intersection[6]);
          break;
        case west:
          ++(vehicles_in_intersection[7]);
          break;
        case east:
          ++(vehicles_in_intersection[8]);
          break;
        case south:
          break;
      }
      break;

    case west:
      switch (destination) {
        case east:
          ++(vehicles_in_intersection[9]);
          break;
        case north:
          ++(vehicles_in_intersection[10]);
          break;
        case south:
          ++(vehicles_in_intersection[11]);
          break;
        case west:
          break;
      }
      break;
  
  }
}


/*
 * This function decrements the element in the array that stores
 * the number of cars going in a particular direction.
 */

static void
exit_intersection(Direction origin, Direction destination)
{
  switch (origin) {
    
    case north:
      switch (destination) {
        case south:
          --(vehicles_in_intersection[0]);
          break;
        case west:
          --(vehicles_in_intersection[1]);
          break;
        case east:
          --(vehicles_in_intersection[2]);
          break;
        case north:
          break;
      }
      break;
    
    case east:
      switch (destination) {
        case west:
          --(vehicles_in_intersection[3]);
          break;
        case north:
          --(vehicles_in_intersection[4]);
          break;
        case south:
          --(vehicles_in_intersection[5]);
          break;
        case east:
          break;
      }
      break;

    case south:
      switch (destination) {
        case north:
          --(vehicles_in_intersection[6]);
          break;
        case west:
          --(vehicles_in_intersection[7]);
          break;
        case east:
          --(vehicles_in_intersection[8]);
          break;
        case south:
          break;
      }
      break;

    case west:
      switch (destination) {
        case east:
          --(vehicles_in_intersection[9]);
          break;
        case north:
          --(vehicles_in_intersection[10]);
          break;
        case south:
          --(vehicles_in_intersection[11]);
          break;
        case west:
          break;
      }
      break;
  
  }
}


/*
 * This function checks if a vehicle has a different destination
 * than a vehicle in the intersection.
 */

static bool
different_destination(Direction destination1, Direction destination2)
{
  if (destination1 != destination2) {
    return true;
  } else {
    return false;
  }
}


/*
 * This function checks if a vehicle is making a right turn.
 */

static bool
right_turn(Direction origin, Direction destination)
{
  if (((origin == north) && (destination == west)) ||
      ((origin == east) && (destination == north)) ||
      ((origin == south) && (destination == east)) ||
      ((origin == west) && (destination == south))) {
    return true; 
  } else {
    return false;
  }
}


/*
 * This function checks if a vehicle is going in the opposite direction
 * as a vehicle in the intersection.
 */

static bool
opposite_direction(Direction origin1, Direction destination1,
                   Direction origin2, Direction destination2)
{
  if ((origin1 == destination2) && (destination1 == origin2)) {
    return true;
  } else {
    return false;
  }
}


/*
 * This function checks if a vehicle is entering the intersection from
 * the same direction as a vehicle in the intersection.
 */

static bool
same_direction(Direction origin1, Direction origin2)
{
  if (origin1 == origin2) {
    return true;
  } else {
    return false;
  }
}


/*
 * This function checks if a vehicle entering the intersection meets
 * the synchronization conditions.
 */

static bool
conditions_met(Direction origin, Direction destination)
{
  for (unsigned int i = 0; i < num_directions; ++i) {
    if (vehicles_in_intersection[i] > 0) {
      Direction cur_origin = directions[i][0];
      Direction cur_destination = directions[i][1];
      if ((!same_direction(origin, cur_origin)) &&
          (!opposite_direction(origin, destination, cur_origin, cur_destination)) &&
          (!((right_turn(cur_origin, cur_destination) ||
             (right_turn(origin, destination))) &&
              different_destination(destination, cur_destination)))) {
        return false;
      }
    }
  }
  return true;
}


/* 
 * The simulation driver will call this function once before starting
 * the simulation.
 *
 * You can use it to initialize synchronization and other variables.
 */

void
intersection_sync_init(void)
{
  mutex = lock_create("mutex");
  if (mutex == NULL) {
    panic("could not create mutex");
  }
  
  north_south_can_enter = cv_create("north-south can enter");
  if (north_south_can_enter == NULL) {
    panic("could not create north-south can enter condition variable");
  }

  north_west_can_enter = cv_create("north-west can enter");
  if (north_west_can_enter == NULL) {
    panic("could not create north-west can enter condition variable");
  }

  north_east_can_enter = cv_create("north-east can enter");
  if (north_east_can_enter == NULL) {
    panic("could not create north-east can enter condition variable");
  }

  east_west_can_enter = cv_create("east-west can enter");
  if (east_west_can_enter == NULL) {
    panic("could not create east-west can enter condition variable");
  }

  east_north_can_enter = cv_create("east-north can enter");
  if (east_north_can_enter == NULL) {
    panic("could not create east-north can enter condition variable");
  }

  east_south_can_enter = cv_create("east-south can enter");
  if (east_south_can_enter ==  NULL) {
    panic("could not create east-south can enter condition variable");
  }

  south_north_can_enter = cv_create("south-north can enter");
  if (south_north_can_enter == NULL) {
    panic("could not create south-north can enter condition variable");
  }

  south_west_can_enter = cv_create("south-west can enter");
  if (south_west_can_enter == NULL) {
    panic("could not create south-west can enter condition variable");
  }

  south_east_can_enter = cv_create("south-east can enter");
  if (south_east_can_enter == NULL) {
    panic("could not create south-east can enter condition variable");
  }

  west_east_can_enter = cv_create("west-east can enter");
  if (west_east_can_enter == NULL) {
    panic("could not create west-east can enter condition variable");
  }

  west_north_can_enter = cv_create("west-north can enter");
  if (west_north_can_enter == NULL) {
    panic("could not create west-north can enter condition variable");
  }

  west_south_can_enter = cv_create("west-south can enter");
  if (west_south_can_enter == NULL) {
    panic("could not create west-south can enter condition variable");
  }
}


/* 
 * The simulation driver will call this function once after
 * the simulation has finished.
 *
 * You can use it to clean up any synchronization and other variables.
 */

void
intersection_sync_cleanup(void)
{
  KASSERT(west_south_can_enter != NULL);
  KASSERT(west_north_can_enter != NULL);
  KASSERT(west_east_can_enter != NULL);
  KASSERT(south_east_can_enter != NULL);
  KASSERT(south_west_can_enter != NULL);
  KASSERT(south_north_can_enter != NULL);
  KASSERT(east_south_can_enter != NULL);
  KASSERT(east_north_can_enter != NULL);
  KASSERT(east_west_can_enter != NULL);
  KASSERT(north_east_can_enter != NULL);
  KASSERT(north_west_can_enter != NULL);
  KASSERT(north_south_can_enter != NULL);
  KASSERT(mutex != NULL);
  
  lock_destroy(mutex);
  cv_destroy(west_south_can_enter);
  cv_destroy(west_north_can_enter);
  cv_destroy(west_east_can_enter);
  cv_destroy(south_east_can_enter);
  cv_destroy(south_west_can_enter);
  cv_destroy(south_north_can_enter);
  cv_destroy(east_south_can_enter);
  cv_destroy(east_north_can_enter);
  cv_destroy(east_west_can_enter);
  cv_destroy(north_east_can_enter);
  cv_destroy(north_west_can_enter);
  cv_destroy(north_south_can_enter);
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * Parameters:
 *    * Origin: the Direction from which the vehicle is arriving
 *    * Destination: the Direction in which the vehicle is trying to go
 *
 * Return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination) 
{
  KASSERT(mutex != NULL);
  KASSERT(north_south_can_enter != NULL);
  KASSERT(north_west_can_enter != NULL);
  KASSERT(north_east_can_enter != NULL);
  KASSERT(east_west_can_enter != NULL);
  KASSERT(east_north_can_enter != NULL);
  KASSERT(east_south_can_enter != NULL);
  KASSERT(south_north_can_enter != NULL);
  KASSERT(south_west_can_enter != NULL);
  KASSERT(south_east_can_enter != NULL);
  KASSERT(west_east_can_enter != NULL);
  KASSERT(west_north_can_enter != NULL);
  KASSERT(west_south_can_enter != NULL);

  lock_acquire(mutex);
  
  while (!conditions_met(origin, destination)) {
    switch (origin) {
    
      case north:
        switch (destination) {
          case south:
            cv_wait(north_south_can_enter, mutex);
            break;
          case west:
            cv_wait(north_west_can_enter, mutex);
            break;
          case east:
            cv_wait(north_east_can_enter, mutex);
            break;
          case north:
            break;
        }
        break;
    
      case east:
        switch (destination) {
          case west:
            cv_wait(east_west_can_enter, mutex);
            break;
          case north:
            cv_wait(east_north_can_enter, mutex);
            break;
          case south:
            cv_wait(east_south_can_enter, mutex);
            break;
          case east:
            break;
          }
        break;

      case south:
        switch (destination) {
          case north:
            cv_wait(south_north_can_enter, mutex);
            break;
          case west:
            cv_wait(south_west_can_enter, mutex);
            break;
          case east:
            cv_wait(south_east_can_enter, mutex);
            break;
          case south:
            break;
        }
        break;

      case west:
        switch (destination) {
          case east:
            cv_wait(west_east_can_enter, mutex);
            break;
          case north:
            cv_wait(west_north_can_enter, mutex);
            break;
          case south:
            cv_wait(west_south_can_enter, mutex);
            break;
          case west:
            break;
        }
        break;
  
    }
  }
  enter_intersection(origin, destination);
  
  lock_release(mutex);
}


/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * Parameters:
 *    * Origin: the Direction from which the vehicle arrived
 *    * Destination: the Direction in which the vehicle is going
 *
 * Return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination) 
{
  KASSERT(mutex != NULL);
  KASSERT(north_south_can_enter != NULL);
  KASSERT(north_west_can_enter != NULL);
  KASSERT(north_east_can_enter != NULL);
  KASSERT(east_west_can_enter != NULL);
  KASSERT(east_north_can_enter != NULL);
  KASSERT(east_south_can_enter != NULL);
  KASSERT(south_north_can_enter != NULL);
  KASSERT(south_west_can_enter != NULL);
  KASSERT(south_east_can_enter != NULL);
  KASSERT(west_east_can_enter != NULL);
  KASSERT(west_north_can_enter != NULL);
  KASSERT(west_south_can_enter != NULL);

  lock_acquire(mutex);
  
  exit_intersection(origin, destination);
  if (conditions_met(north, south)) {
    cv_broadcast(north_south_can_enter, mutex);
  } 
  if (conditions_met(north, west)) {
    cv_broadcast(north_west_can_enter, mutex);
  }
  if (conditions_met(north, east)) {
    cv_broadcast(north_east_can_enter, mutex);
  }
  if (conditions_met(east, west)) {
    cv_broadcast(east_west_can_enter, mutex);
  }
  if (conditions_met(east, north)) {
    cv_broadcast(east_north_can_enter, mutex);
  }
  if (conditions_met(east, south)) {
    cv_broadcast(east_south_can_enter, mutex);
  }
  if (conditions_met(south, north)) {
    cv_broadcast(south_north_can_enter, mutex);
  }
  if (conditions_met(south, west)) {
    cv_broadcast(south_west_can_enter, mutex);
  }
  if (conditions_met(south, east)) {
    cv_broadcast(south_east_can_enter, mutex);
  }
  if (conditions_met(west, east)) {
    cv_broadcast(west_east_can_enter, mutex);
  }
  if (conditions_met(west, north)) {
    cv_broadcast(west_north_can_enter, mutex);
  }
  if (conditions_met(west, south)) {
    cv_broadcast(west_south_can_enter, mutex);
  }
  
  lock_release(mutex);
}
