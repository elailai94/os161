#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <mips/trapframe.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <vfs.h>
#include <copyinout.h>
#include <synch.h>
#include <limits.h>
#include "opt-A2.h"

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void
sys__exit(int exitcode)
{

  struct addrspace *as;
  struct proc *p = curproc;

#if OPT_A2
#else
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;
#endif /* OPT_A2 */

  DEBUG(DB_SYSCALL, "Syscall: _exit(%d)\n", exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

#if OPT_A2
  lock_acquire(kproctable->pt_lock);

  /*
   * Remove all the exited children of the current process from the
   * process table. The process IDs of these removed processes can
   * then be safely reused.
   */
  proctable_remexitedchildprocs(p->p_pid);
  /*
   * Update all the running children of the current process in the
   * process table to be orphans.
   */
  proctable_updaterunningchildprocs(p->p_pid);

  if (p->p_parentpid == -1) { /* Current process has no parent? */
     proctable_remproc(p->p_pid);
  } else {
     p->p_exitstate = true;
     p->p_exitcode = _MKWAIT_EXIT(exitcode);
     /* Wakes up the parent of the current process */
     cv_signal(p->p_hasexited, kproctable->pt_lock);
  }

  lock_release(kproctable->pt_lock);
#else
  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
#endif /* OPT_A2 */

  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}

/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
#if OPT_A2
  *retval = curproc->p_pid;
#else
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  *retval = 1;
#endif /* OPT_A2 */

  return(0);
}

/* stub handler for waitpid() system call                */
int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {                     /* Requested invalid or unsupported options? */
    return(EINVAL);
  }

#if OPT_A2
  if (status == NULL) {                   /* Invalid status? */
    return(EFAULT);
  }

  lock_acquire(kproctable->pt_lock);

  struct proc *p = proctable_getproc(pid);
  if (p == NULL) {                        /* Nonexistent process? */
    lock_release(kproctable->pt_lock);
    return(ESRCH);
  }

  if (p->p_parentpid != curproc->p_pid) { /* Current process isn't the parent of process? */
    lock_release(kproctable->pt_lock);
    return(ECHILD);
  }

  /* Block the current process until process exits */
  while (!p->p_exitstate) {
    cv_wait(p->p_hasexited, kproctable->pt_lock);
  }
  exitstatus = p->p_exitcode;

  lock_release(kproctable->pt_lock);
#else
  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;
#endif /* OPT_A2 */

  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#if OPT_A2
/*
 * Duplicate the current process.
 */
int
sys_fork(struct trapframe *tf, pid_t *retval)
{
   int result;

   lock_acquire(kproctable->pt_lock);

   /* Check that there aren't too many processes on the system */
   unsigned int kproctable_len = proctable_getlen();
   if (kproctable_len >= (PID_MAX - PID_MIN + 1)) {
      lock_release(kproctable->pt_lock);
      return(ENPROC);
   }

   /* Create process structure for child process */
   struct proc *cp = proc_create_runprogram("child process");
   if (cp == NULL) {
      lock_release(kproctable->pt_lock);
      return(ENOMEM);
   }

   lock_release(kproctable->pt_lock);

   /* Create and copy address space */
   struct addrspace *pp_as = curproc_getas();
   struct addrspace *cp_as = NULL;
   result = as_copy(pp_as, &cp_as);
   if (result) {
      lock_acquire(kproctable->pt_lock);
      proctable_remproc(cp->p_pid);
      lock_release(kproctable->pt_lock);
      return(result);
   }
   cp->p_addrspace = cp_as;

   /* Create the parent and child relationship */
   cp->p_parentpid = curproc->p_pid;

   /* Create and copy trapframe */
   struct trapframe *cp_tf = kmalloc(sizeof(struct trapframe));
   if (cp_tf == NULL) {
      as_destroy(cp_as);
      lock_acquire(kproctable->pt_lock);
      proctable_remproc(cp->p_pid);
      lock_release(kproctable->pt_lock);
      return(ENOMEM);
   }
   *cp_tf = *tf;

   /* Create thread for child process */
   result = thread_fork("child thread", cp, enter_forked_process,
                        (void *) cp_as /* Copy of address space */,
                        (unsigned long) cp_tf /* Copy of trapframe */);
   if (result) {
      kfree(cp_tf);
      as_destroy(cp_as);
      lock_acquire(kproctable->pt_lock);
      proctable_remproc(cp->p_pid);
      lock_release(kproctable->pt_lock);
      return(result);
   }

   /* Return the process ID of the child process */
   *retval = cp->p_pid;

   return(0);
}

/*
 * Execute a program.
 */
int
sys_execv(userptr_t progname, userptr_t args)
{
   int argc;
   char **argv;
   struct addrspace *old_as;
   struct addrspace *new_as;
   struct vnode *v;
   vaddr_t entrypoint;
   vaddr_t stackptr;
   int result;

   if (progname == NULL) {               /* Invalid pointer to program name? */
      return(EFAULT);
   }

   if (args == NULL) {                   /* Invalid pointer to arguments array? */
      return(EFAULT);
   }

   /* Copy the program path into the kernel */
   char *progpath = kmalloc(sizeof(char) * (strlen((char *) progname) + 1));
   if (progpath == NULL) {
      return(ENOMEM);
   }

   result = copyinstr((const_userptr_t) progname, progpath,
                      PATH_MAX, NULL);
   if (result) {
      kfree(progpath);
      return(result);
   }

   /* Count the number of arguments */
   argc = 0;
   argv = (char **) args;
   while (argv[argc] != NULL) {
      argc++;
   }

   /* Copy arguments into the kernel */
   argv = kmalloc(sizeof(char *) * (argc + 1));
   if (argv == NULL) {
      kfree(progpath);
      return(ENOMEM);
   }

   size_t argssize = 0;
   for (int i = 0; i < argc; ++i) {
      char **argstemp = (char **) args;

      size_t argsize = (sizeof(char) * (strlen(argstemp[i]) + 1));
      argssize += argsize;
      if (argssize > ARG_MAX) { /* Total size of argument strings is too large? */
         for (int j = 0; j < i; ++j) {
            kfree(argv[j]);
         }
         kfree(argv);
         kfree(progpath);
         return(E2BIG);
      }

      argv[i] = kmalloc(argsize);
      if (argv[i] == NULL) {
         for (int j = 0; j < i; ++j) {
            kfree(argv[j]);
         }
         kfree(argv);
         kfree(progpath);
         return(ENOMEM);
      }

      result = copyinstr((const_userptr_t) argstemp[i],
                         argv[i], 1024, NULL);
      if (result) {
         for (int j = 0; j <= i; ++j) {
            kfree(argv[j]);
         }
         kfree(argv);
         kfree(progpath);
         return(result);
      }
   }
   argv[argc] = NULL;

   /* Open the file */
   char *progpath_temp = kstrdup(progpath);
   if (progpath_temp == NULL) {
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(ENOMEM);
   }
   result = vfs_open(progpath_temp, O_RDONLY, 0, &v);
   kfree(progpath_temp);
   if (result) {
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(result);
   }

   /* Get reference to old address space */
   old_as = curproc_getas();

   /* Create a new address space */
   new_as = as_create();
   if (new_as == NULL) {
      vfs_close(v);
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(ENOMEM);
   }

   /* Switch to it and activate it */
   curproc_setas(new_as);
   as_activate();

   /* Load the executable */
   result = load_elf(v, &entrypoint);
   if (result) {
      /* p_addrspace will go away when curproc is destroyed */
      vfs_close(v);
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(result);
   }

   /* Done with file now */
   vfs_close(v);

   /* Define the user stack in the address space */
   result = as_define_stack(new_as, &stackptr);
   if (result) {
      /* p_addrspace will go away when curproc is destroyed */
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(result);
   }

   /* Copy arguments into the new address space */
   char **argsarr = kmalloc(sizeof(char *) * (argc + 1));
   if (argsarr == NULL) {
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(ENOMEM);
   }

   for (int i = 0; i < argc; ++i) {
      size_t arglen = (strlen(argv[i]) + 1);
      size_t arglenaligned = ROUNDUP(arglen, 8);
      stackptr -= arglenaligned;
      argsarr[i] = (char *) stackptr;
      result = copyoutstr(argv[i], (userptr_t) stackptr, 1024, NULL);
      if (result) {
        kfree(argsarr);
        for (int i = 0; i < argc; ++i) {
           kfree(argv[i]);
        }
        kfree(argv);
        kfree(progpath);
        return(result);
      }
   }
   argsarr[argc] = NULL;

   size_t argsarrlen = (sizeof(char *) * (argc + 1));
   size_t argsarrlenaligned = ROUNDUP(argsarrlen, 8);
   stackptr -= argsarrlenaligned;
   result = copyout(argsarr, (userptr_t) stackptr, argsarrlen);
   if (result) {
      kfree(argsarr);
      for (int i = 0; i < argc; ++i) {
         kfree(argv[i]);
      }
      kfree(argv);
      kfree(progpath);
      return(result);
   }

   /* Delete old address space */
   as_destroy(old_as);

   /* Clean up memory allocated for some temporary variables */
   kfree(argsarr);
   for (int i = 0; i < argc; ++i) {
      kfree(argv[i]);
   }
   kfree(argv);
   kfree(progpath);

   /* Warp to user mode. */
   enter_new_process(argc /*argc*/, (userptr_t) stackptr /*userspace addr of argv*/,
                     stackptr, entrypoint);

   /* enter_new_process does not return */
   panic("enter_new_process returned\n");
   return(EINVAL);
}
#endif /* OPT_A2 */
